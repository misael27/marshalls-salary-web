import {Component, Inject, OnInit} from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from "@angular/forms";
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Salary } from '../../../common/entities/salary';

@Component({
  selector: 'app-add-salary',
  templateUrl: './add-salary.component.html',
  styleUrls: ['./add-salary.component.scss']
})
export class AddSalaryComponent implements OnInit {

  formGroup: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<AddSalaryComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Salary
  ) { }

  ngOnInit() {
    this.formGroup = this.formBuilder.group({
      id: [this.data ? this.data.id : 0, Validators.required],
      year: [this.data ? this.data.year : '', [Validators.required, Validators.min(1900), Validators.max(2022)]],
      month: [this.data ? this.data.month : '', [Validators.required, Validators.min(1), Validators.max(12)]],
      baseSalary: [this.data ? this.data.baseSalary : '', [Validators.required, Validators.min(0)]],
      productionBonus: [this.data ? this.data.productionBonus : '', [Validators.required, Validators.min(0)]],
      compensatioBonus: [this.data ? this.data.compensatioBonus : '', [Validators.required, Validators.min(0)]],
      commission: [this.data ? this.data.commission : '', [Validators.required, Validators.min(0)]],
      contributions: [this.data ? this.data.contributions : '', [Validators.required, Validators.min(0)]]
    });
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  onAdd() {
    this.dialogRef.close(this.formGroup.value);
  }

  isValid() {
    return this.formGroup.valid;
  }


}
